import cv2
import sys
import time
import queue
import threading
import datetime as dt
import calendar
import os

#Set
#video_source="http://192.168.10.1/videostream.asf?user=admin&pwd=&resolution=1280*720&rate=0"
video_source=1
q = queue.Queue()
img_scale_rate=0.4


def _sysexit():
    cv2.destroyAllWindows
    sys.exit()

def ReadLog(logpath):
    datestr=None
    w=0
    m=0
    if os.path.exists(logpath):
        f = open(logpath, 'r')
        lastline=f.readlines()[-1]
        lsplite=lastline.split(',')
        if len(lsplite)>=3:
            datestr=lsplite[0]
            w=lsplite[1]
            m=lsplite[2].replace('\n','')
            print(datestr,w,m)
    return datestr,w,m

def Receive(time_du):
    print("start Reveive , duration = ",time_du, " second.")
    cap = cv2.VideoCapture(video_source)
    ret, frame = cap.read()
    q.put(frame)
    while ret:
        ret, frame = cap.read()
        #print("Read video")
        q.put(frame)
        time.sleep(time_du)


def SaveFile(time_du,folder,logfile):
    print("Start Saving images , duration = ",time_du, " second.")

    while True:
        if q.empty() != True:
            frame = q.get()

            time_string=dt.datetime.today().strftime("%Y_%m_%d_%H_%M_%S_%f")

            #path = os.path.join(".",folder,str(time_string)+'_w_'+weight.replace('.','dot')+'_m_'+move.replace('.','dot')+'.jpg')
            path = os.path.join(".",folder,str(time_string)+'.jpg')
            print(path)
            cv2.imwrite(path,frame)

            # Writing log/img to a file
            date,weight,move=ReadLog(logfile)
            if date==None:
                continue
            file1 = open(os.path.join(folder,'weight_image.txt'), 'a')
            line=date+","+weight+","+move+","+path
            file1.write(line+'\n')
            file1.close()
            cv2.resizeWindow('image', int(frame.shape[0]*0.5),int(frame.shape[1]*0.5))
            cv2.imshow("frame1", frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

if __name__ == '__main__':
    #Get Weight from user input and start logging
    time_du=input("間隔時間(單位秒, eg. 1 , 0.5 ):")
    log_file=input("重量記錄檔路徑:")

    if os.path.exists(log_file)==False:
        print(log_file," 此路徑不存在, 請重新確認")
        _sysexit()


    time_string=dt.datetime.today().strftime("%Y_%m_%d_%H_%M_%S")
    if (os.path.exists(os.path.join(".",time_string)))==False:
        os.mkdir( os.path.join(".",time_string), 755 )
        print("建立目錄:",os.path.join(".",time_string))

    p1 = threading.Thread(target=Receive,args=(float(time_du),))
    p2 = threading.Thread(target=SaveFile,args=(float(time_du),time_string,log_file,))
    p1.start()
    p2.start()


